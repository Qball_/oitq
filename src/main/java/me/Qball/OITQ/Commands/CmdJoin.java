package me.Qball.OITQ.Commands;

import me.Qball.OITQ.Core;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;

public class CmdJoin implements CommandExecutor {

    private final Core plugin;

    public CmdJoin(Core plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("OITQ")) {
            if (sender instanceof Player) {
                Player p = (Player) sender;
                if (args.length <= 0) {
                    p.sendMessage(ChatColor.RED + "You must do either OITQ join or leave");
                } else if (args.length >= 1) {
                    if (args[0].equalsIgnoreCase("join")) {
                        if (!Core.inGame) {
                            if (!Core.players.contains(p.getUniqueId())) {
                                Core.players.add(p.getUniqueId());
                                Core.scores.put(p.getUniqueId(), 0);
                                Core.noDam.add(p.getUniqueId());
                                p.sendMessage(ChatColor.GREEN + "You have joined the game");
                            } else {
                                p.sendMessage(ChatColor.RED + "You have already joined the game");
                            }
                        } else {
                            p.sendMessage(ChatColor.RED + "There is a game in progress already");
                        }
                    } else if (args[0].equalsIgnoreCase("leave")) {
                        if (Core.players.contains(p.getUniqueId())) {
                            Core.players.remove(p.getUniqueId());
                            Core.scores.remove(p.getUniqueId());
                            Core.noDam.remove(p.getUniqueId());
                            String spawnLoc = plugin.getConfig().getString("HubSpawn");
                            String[] spawn = spawnLoc.split(":");
                            String world = spawn[0];
                            int x = Integer.parseInt(spawn[1]);
                            int y = Integer.parseInt(spawn[2]);
                            int z = Integer.parseInt(spawn[3]);
                            Location loc = new Location(Bukkit.getWorld(world), x, y, z, 0.0F, 0.0F);
                            p.teleport(loc);
                        } else {
                            p.sendMessage(ChatColor.RED + "You are not in a game");
                        }
                    } else if (args[0].equalsIgnoreCase("start")) {
                        if (p.hasPermission("OITQ.game.status")) {
                            for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                                Core.players.add(player.getUniqueId());
                                Core.scores.put(player.getUniqueId(), 0);
                                Core.noDam.add(player.getUniqueId());
                               // player.sendMessage(ChatColor.GREEN + "You have joined the game");
                            }
                            Core.inGame = true;
                            if(args.length<=2)
                                Core.start();
                            else
                                Core.start(args[1]);
                        }
                    } else if (args[0].equalsIgnoreCase("end")) {
                        if (p.hasPermission("OITQ.game.status")) {
                            Core.inGame = false;
                            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "gamedone oitq");
                            HandlerList.unregisterAll((Plugin) plugin);
                        }
                    }
                    if (args[0].equalsIgnoreCase("set")) {
                        if (args.length >= 2) {
                            if (args[1].equalsIgnoreCase("hubspawn")) {
                                if (p.hasPermission("OITQ.set.spawn")) {
                                    String world = p.getWorld().toString();
                                    String x = String.valueOf(p.getLocation().getX());
                                    String y = String.valueOf(p.getLocation().getY());
                                    String z = String.valueOf(p.getLocation().getZ());
                                    String pitch = String.valueOf(p.getLocation().getPitch());
                                    String yaw = String.valueOf(p.getLocation().getYaw());
                                    String location = world + "," + x + "," + y + "," + z + "," + pitch + "," + yaw;
                                    plugin.getConfig().set("HubSpawn", location);
                                    plugin.saveConfig();
                                    plugin.reloadConfig();
                                    p.sendMessage(ChatColor.GREEN + "You have set the Hub Spawn");
                                }
                            } else if (args[1].equalsIgnoreCase("gameSpawn")) {
                                if (p.hasPermission("OITQ.set.spawn")) {
                                    String world = p.getWorld().toString();
                                    String x = String.valueOf(p.getLocation().getX());
                                    String y = String.valueOf(p.getLocation().getY());
                                    String z = String.valueOf(p.getLocation().getZ());
                                    String pitch = String.valueOf(p.getLocation().getPitch());
                                    String yaw = String.valueOf(p.getLocation().getYaw());
                                    String location = world + "," + x + "," + y + "," + z + "," + pitch + "," + yaw;
                                    plugin.getConfig().set("GameSpawn", location);
                                    plugin.saveConfig();
                                    plugin.reloadConfig();
                                    p.sendMessage(ChatColor.GREEN + "You have set the Game Spawn");
                                }
                            }
                        }
                    }
                }
            }
            else{
                if(args.length>=1) {
                    sender.sendMessage("k");
                    if (args[0].equalsIgnoreCase("start")) {
                        sender.sendMessage("start");
                        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                            Core.players.add(player.getUniqueId());
                            Core.scores.put(player.getUniqueId(), 0);
                            Core.noDam.add(player.getUniqueId());
                            // player.sendMessage(ChatColor.GREEN + "You have joined the game");
                        }
                        Core.inGame = true;
                        if (args.length <= 2)
                            Core.start();
                        else
                            Core.start(args[1]);
                    } else if (args[0].equalsIgnoreCase("end")) {
                        Core.inGame = false;
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "gamedone oitq");
                        HandlerList.unregisterAll((Plugin) plugin);
                    }
                }
        }

        }
        return false;
    }

    public Core getPlugin() {
        return plugin;
    }

}
