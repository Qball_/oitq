package me.Qball.OITQ;

import me.Qball.OITQ.Commands.CmdJoin;
import me.Qball.OITQ.Listeners.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class Core extends JavaPlugin implements Listener {
    public static HashMap<UUID, Integer> scores = new HashMap<>();
    public static ArrayList<UUID> players = new ArrayList<>();
    public static ArrayList<UUID> noDam = new ArrayList<>();
    public static boolean inGame = false;
    private static Core instance;

    public static void start(String mapName) {
        instance.registerEvents();
        Bukkit.getServer().broadcastMessage(ChatColor.RED + "The game has started there is a 5 second grace period to run about. First to 15 kills wins");
        ItemStack bow = new ItemStack(Material.BOW, 1);
        ItemStack arrow = new ItemStack(Material.ARROW, 1);
        ItemStack sword = new ItemStack(Material.IRON_SWORD, 1);
        for (Player p : Bukkit.getServer().getOnlinePlayers()){
            Location loc = getRandomLocation(mapName);
            noDam.add(p.getUniqueId());
            p.teleport(loc);
            p.getInventory().clear();
            p.getInventory().setItem(0, sword);
            p.getInventory().setItem(1, bow);
            p.getInventory().setItem(2, arrow);
            p.sendMessage(ChatColor.GREEN + "There is a 5 second grace peroid");
        }
        new BukkitRunnable() {
            public void run() {
                noDam.clear();
                Bukkit.getServer().broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + "Grace period is over. Let games begin");

            }
        }.runTaskLater(instance, 100);

    }
    public static void start(){
        start("OITQ3");
    }
    public static void gameStart() {
        if (scores.size() == instance.getConfig().getInt("MinPlay") || inGame) {
            inGame = true;
            String spawn = instance.getConfig().getString("GameSpawn");
            String[] reSpawn = spawn.split(",");
            String world = reSpawn[0];
            String x = reSpawn[1];
            String y = reSpawn[2];
            String z = reSpawn[3];
            String pitch = reSpawn[4];
            String yaw = reSpawn[5];
            Bukkit.getServer().broadcastMessage(ChatColor.RED + "The game has started there is a 10 second grace period to run about");
            /*final Location loc = new Location(
                    Bukkit.getWorld(world),
                    Double.parseDouble(x),
                    Double.parseDouble(y),
                    Double.parseDouble(z),
                    Float.parseFloat(pitch),
                    Float.parseFloat(yaw));*/
            Location loc = getRandomLocation(world);
            for (int i = 0; i < +Core.players.size(); i++) {
                Player p = Bukkit.getServer().getPlayer(Core.players.get(i));
                p.teleport(loc);
            }
            new BukkitRunnable() {
                public void run() {
                    noDam.clear();
                    Bukkit.getServer().broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + "Grace period is over. Lets game begin");

                }
            }.runTaskLater(instance, 200);
        }

    }

    public static Core getInstance() {
        return instance;
    }

    public void onDisable() {
    }

    private void registerEvents() {
        Bukkit.getServer().getPluginManager().registerEvents(new ArrowHit(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new ArrowPickUp(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new DeathEvent(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new DropItem(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new NoItemDam(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new BowShot(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new DamageCancel(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerDisconnect(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new InvClick(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new NoBreak(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new ProjectileHit(),this);
        Bukkit.getServer().getPluginManager().registerEvents(new ScoreChecker(), this);
    }

    public void onEnable() {
        instance = this;
        this.getCommand("OITQ").setExecutor(new CmdJoin(this));
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
       // ScoreListener score = new ScoreListener();
        //score.endGame();
    }

    public static Location getRandomLocation(String mapName){
        Random random = getRandom();
        int minX = getInstance().getConfig().getInt("GameSpawn."+mapName+".MinX");
        int maxX = getInstance().getConfig().getInt("GameSpawn."+mapName+".MaxX");
        int minZ = getInstance().getConfig().getInt("GameSpawn."+mapName+".MinZ");
        int maxZ = getInstance().getConfig().getInt("GameSpawn."+mapName+".MaxZ");
        int x = random.nextInt(maxX-minX+1)+minX;
        int z = random.nextInt(maxZ-minZ+1)+minZ;
        int y = Bukkit.getWorld(mapName).getHighestBlockYAt(x,z);
        Location loc = new Location(Bukkit.getWorld(mapName),x,y,z,0F,0F);
        if(loc.getBlock().isLiquid()||loc.getBlock().equals(Material.LEAVES))
            getRandomLocation(mapName);
        else
            return new Location(Bukkit.getWorld(mapName),x,y,z,0F,0F);
        return null;
    }
    private static Random getRandom(){
        return new Random();
    }
}
