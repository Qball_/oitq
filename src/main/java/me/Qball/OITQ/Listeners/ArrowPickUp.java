package me.Qball.OITQ.Listeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

/**
 * Created by pi on 08/01/17.
 */
public class ArrowPickUp implements Listener {
    @EventHandler
    public void onPickUp(PlayerPickupItemEvent e) {
        if (e.getItem().getItemStack().getType().equals(Material.ARROW))
            e.setCancelled(true);
    }
}
