package me.Qball.OITQ.Listeners;


import me.Qball.OITQ.Core;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerDisconnect implements Listener {
    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        if (Core.noDam.contains(e.getPlayer().getUniqueId())) {
            Core.noDam.remove(e.getPlayer().getUniqueId());
        }
        if (Core.players.contains(e.getPlayer().getUniqueId())) {
            Core.players.remove(e.getPlayer().getUniqueId());
        }
        if (Core.scores.containsKey(e.getPlayer().getUniqueId())) {
            Core.scores.remove(e.getPlayer().getUniqueId());
        }
        if(Bukkit.getServer().getOnlinePlayers().size()==1){
            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),"gamedone oitq");
        }
    }
}
