package me.Qball.OITQ.Listeners;

import me.Qball.OITQ.Core;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by pi on 4/12/17.
 */
public class PlayerJoin implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        if(Core.inGame){
            if(!Core.players.contains(e.getPlayer().getUniqueId())){
                for(Player p : Bukkit.getServer().getOnlinePlayers()){
                    if(Core.players.contains(p.getUniqueId())){
                        ItemStack bow = new ItemStack(Material.BOW, 1);
                        ItemStack arrows = new ItemStack(Material.ARROW, 1);
                        ItemStack sword = new ItemStack(Material.IRON_SWORD, 1);
                        e.getPlayer().teleport(Core.getRandomLocation(p.getLocation().getWorld().getName()));
                        e.getPlayer().getInventory().clear();
                        e.getPlayer().getInventory().setItem(0, sword);
                        e.getPlayer().getInventory().setItem(1, bow);
                        e.getPlayer().getInventory().setItem(2, arrows);
                        Core.players.add(e.getPlayer().getUniqueId());
                        break;
                    }
                }
            }
        }
    }
}
