package me.Qball.OITQ.Listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by pi on 3/28/17.
 */
public class ScoreEvent extends Event {

    public static final HandlerList handlers = new HandlerList();
    private Player player;
    public ScoreEvent(Player player) {
        this.player = player;
    }
    public Player getPlayer(){
        return this.player;
    }
    public static HandlerList getHandlerList() {
       return handlers;
    }
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

}
