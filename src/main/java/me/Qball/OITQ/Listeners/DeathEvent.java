package me.Qball.OITQ.Listeners;

import me.Qball.OITQ.Core;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

public class DeathEvent implements Listener {
    @EventHandler
    public void onDeath(EntityDeathEvent e) {
        ItemStack bow = new ItemStack(Material.BOW, 1);
        ItemStack arrow = new ItemStack(Material.ARROW, 1);
        ItemStack sword = new ItemStack(Material.IRON_SWORD, 1);

        if (Core.scores.containsKey(e.getEntity().getUniqueId())) {
            e.getEntity().setHealth(e.getEntity().getMaxHealth());
            e.getDrops().clear();
            Integer score = Core.scores.get(e.getEntity().getKiller().getUniqueId()) + 1;
            Core.scores.put(e.getEntity().getKiller().getUniqueId(), score);
            Player killer = e.getEntity().getKiller();
            killer.sendMessage(ChatColor.GREEN + "Your score is " + score);
            killer.getInventory().addItem(arrow);
            Player killed = (Player) e.getEntity();
            killed.getInventory().clear();
            killed.getInventory().setItem(0, sword);
            killed.getInventory().setItem(1, bow);
            killed.getInventory().setItem(2, arrow);
            Location loc = Core.getRandomLocation(e.getEntity().getLocation().getWorld().getName());
            Bukkit.getServer().broadcastMessage(ChatColor.GREEN+killed.getDisplayName()+ " was killed by " + killer.getDisplayName());
            killed.teleport(loc);
            Bukkit.getServer().getPluginManager().callEvent(new ScoreEvent(killer));
        }
    }
}
