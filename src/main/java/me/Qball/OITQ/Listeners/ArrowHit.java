package me.Qball.OITQ.Listeners;

import me.Qball.OITQ.Core;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;

public class ArrowHit implements Listener {
    @EventHandler
    public void arrowHit(EntityDamageByEntityEvent e) {
        DamageCause ent = e.getCause();
        if (ent == DamageCause.PROJECTILE) {
            Projectile p = (Projectile) e.getDamager();
            if (p instanceof Arrow) {
                e.setDamage(0D);
                Arrow arrow = (Arrow) p;
                Player play = (Player) arrow.getShooter();
                if(arrow.getShooter().equals(e.getEntity())){
                    play.getInventory().addItem(new ItemStack(Material.ARROW));
                    return;
                }
                play.getInventory().addItem(new ItemStack(Material.ARROW, 1));
                Integer score = Core.scores.get(play.getUniqueId()) + 1;
                Core.scores.entrySet().remove(play.getUniqueId());
                Core.scores.put(play.getUniqueId(), score);
                play.sendMessage(ChatColor.GREEN + "Your score is " + score);
                arrow.remove();
                Location loc = Core.getRandomLocation(e.getEntity().getLocation().getWorld().getName());
                ItemStack bow = new ItemStack(Material.BOW, 1);
                ItemStack arrows = new ItemStack(Material.ARROW, 1);
                ItemStack sword = new ItemStack(Material.IRON_SWORD, 1);
                if (e.getEntity() instanceof Player) {
                    Player player = (Player) e.getEntity();
                    player.getInventory().clear();
                    player.getInventory().setItem(0, sword);
                    player.getInventory().setItem(1, bow);
                    player.getInventory().setItem(2, arrows);
                    player.setHealth(player.getMaxHealth());
                    Bukkit.getServer().broadcastMessage(ChatColor.GREEN+player.getDisplayName()+" was killed by " + ((Player) arrow.getShooter()).getDisplayName());
                }
                e.getEntity().teleport(loc);
                Bukkit.getServer().getPluginManager().callEvent(new ScoreEvent(play));
                e.setCancelled(true);
            }
        }
    }

}
