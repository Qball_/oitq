package me.Qball.OITQ.Listeners;

import me.Qball.OITQ.Core;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import java.util.UUID;

/**
 * Created by pi on 3/31/17.
 */
public class ScoreChecker implements Listener {
    @EventHandler
    public void onDeath(ScoreEvent e){
        UUID uuid = e.getPlayer().getUniqueId();
        if(Core.scores.get(uuid)>=15){
            String p = Bukkit.getPlayer(uuid).getDisplayName();
            Bukkit.getServer().broadcastMessage(
                    ChatColor.GREEN + "Player " + p
                            + " has won the game");
            Core.noDam = Core.players;
            Core.inGame = false;
            Core.scores.clear();
            Core.players.clear();
            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "gamedone oitq");
            HandlerList.unregisterAll((Plugin) Core.getInstance());
            Bukkit.getServer().broadcastMessage(ChatColor.GREEN+ e.getPlayer().getDisplayName()+" has won the game");
            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),"coinadmin add "+ e.getPlayer().getDisplayName() + " 50");
        }
    }
}
