package me.Qball.OITQ.Listeners;

import me.Qball.OITQ.Core;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class NoBreak implements Listener {
    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        if (Core.players.contains(e.getPlayer().getUniqueId())) {
            e.setCancelled(true);
        }
    }

}
