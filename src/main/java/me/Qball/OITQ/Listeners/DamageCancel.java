package me.Qball.OITQ.Listeners;

import me.Qball.OITQ.Core;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class DamageCancel implements Listener {
    @EventHandler
    public void noDamage(EntityDamageEvent e) {
        if (Core.noDam.contains(e.getEntity().getUniqueId())) {
            e.setCancelled(true);
        }
        if (e.getCause().equals(DamageCause.FALL) && Core.players.contains(e.getEntity().getUniqueId())) {
            e.setCancelled(true);
        }
    }

}
