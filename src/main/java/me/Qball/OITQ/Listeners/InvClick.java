package me.Qball.OITQ.Listeners;

import me.Qball.OITQ.Core;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InvClick implements Listener {
    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (Core.players.contains(e.getWhoClicked().getUniqueId())) {
            e.setCancelled(true);
        }
    }
}
