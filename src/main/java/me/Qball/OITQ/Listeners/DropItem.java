package me.Qball.OITQ.Listeners;

import me.Qball.OITQ.Core;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class DropItem implements Listener {

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        if (Core.players.contains(e.getPlayer().getUniqueId())) {
            e.setCancelled(true);
        }
    }

}
